# Phone book



## Description
It is a simple application where you can manage users and their contact data.

## Main technologies
- Spring Boot
- GraphQL
- H2 Database
- Bootstrap

## Application URLS
- / - main page
- /graphiql - GraphQL query tool
- /h2-console - Here you can manage H2 database(JDBC URL: jdbc:h2:mem:phonebook, User Name:admin, Password:phonebook)
