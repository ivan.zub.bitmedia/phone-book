package bitmedia.GraphQL.phonebook.base.controller;

import bitmedia.GraphQL.phonebook.base.graphQL.input.NewUserInput;
import bitmedia.GraphQL.phonebook.base.graphQL.input.UpdateUserInput;
import bitmedia.GraphQL.phonebook.base.graphQL.output.OperationResultOutput;
import bitmedia.GraphQL.phonebook.base.model.User;
import bitmedia.GraphQL.phonebook.base.repository.UserRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class UserController {

  private final UserRepository userRepository;

  @QueryMapping(name = "getAllUsers")
  public List<User> getAllUsers() {
    return (List<User>) userRepository.findAll();
  }

  @MutationMapping(name = "newUser")
  public User createNewUser(@Argument NewUserInput data) {
    User newUser = User.builder().name(data.getName()).email(data.getEmail()).phone(data.getPhone())
        .build();
    userRepository.save(newUser);
    return newUser;
  }

  @MutationMapping(name = "deleteUser")
  public OperationResultOutput deleteUser(@Argument Long data) {
    var user = userRepository.findById(data).orElse(null);
    if (user != null) {
      userRepository.delete(user);
      return OperationResultOutput.builder()
          .success(true)
          .responseCode(0)
          .build();
    } else {
      return OperationResultOutput.builder()
          .success(false)
          .responseCode(-1)
          .build();
    }
  }

  @MutationMapping(name = "updateUser")
  public OperationResultOutput updateUser(@Argument UpdateUserInput data) {
    var user = userRepository.findById(data.getId()).orElse(null);
    if (user != null) {
      user.setName(data.getName());
      user.setEmail(data.getEmail());
      user.setPhone(data.getPhone());
      userRepository.save(user);
      return OperationResultOutput.builder()
          .success(true)
          .responseCode(0)
          .build();
    } else {
      return OperationResultOutput.builder()
          .success(false)
          .responseCode(-1)
          .build();
    }
  }

  @PostMapping("/users")
  void addUser(@RequestBody User user) {
    userRepository.save(user);
  }
}
