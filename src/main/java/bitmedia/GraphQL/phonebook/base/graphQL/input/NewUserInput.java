package bitmedia.GraphQL.phonebook.base.graphQL.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUserInput {

  private String name;
  private String email;
  private String phone;
}
