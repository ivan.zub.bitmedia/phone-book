package bitmedia.GraphQL.phonebook.base.graphQL.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserInput {

  private long id;
  private String name;
  private String email;
  private String phone;
}
