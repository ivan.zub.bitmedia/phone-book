package bitmedia.GraphQL.phonebook.base.repository;

import bitmedia.GraphQL.phonebook.base.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
