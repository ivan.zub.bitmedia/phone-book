package bitmedia.GraphQL.phonebook.web.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PageController {

  @RequestMapping("/")
  public String getMainPage() throws IOException {
    return Files.readString(
        Path.of("src/main/resources/templates/index.html"),
        StandardCharsets.US_ASCII);
  }
}
