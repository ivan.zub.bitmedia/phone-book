function graphQLQueryConstructor(name, params, responseFields) {
  return 'query { ' + name + (params ? ('(' + Array.from(params).map(
          p => p[0] + ': ' + '\"' + p[1] + '\"').join(' ') + ')') : '') + '{ '
      + responseFields.join(
          ' ') + ' } ' + ' } ';
}

function graphQLMutationConstructor(name, paramsObj, responseFields,
    singleParam) {
  return 'mutation { ' + name + '(data:' + (singleParam ? singleParam : ('{'
          + Array.from(paramsObj).map(
              p => p[0] + ': ' + '\"' + p[1] + '\"').join(', ') + '}')) + '){'
      + responseFields.join(
          ' ') + ' }}'
}

function executeQuery(query) {
  let headers = new Headers();
  let body = JSON.stringify({
    query: query
  });
  let response;
  headers.append('Content-Type', 'application/json');
  return fetch('/graphql', {
    method: 'POST',
    headers: headers,
    body: body
  })
  .then(response => {
    if (response.status !== 200) {
      throw 'Smth went wrong!'
    } else {
      return response.json();
    }
  })
  .then(data => {
    return data.data
  })
}

function getHTMLElementValue(id) {
  return document.getElementById(id).value;
}