let usersTable = undefined;

$(document).ready(function () {
  usersTable = document.getElementById("usersTable");
  getAllUsers();
});

function fillFormWithRandomData() {
  let randomName = names[Math.floor(Math.random() * names.length)]
  document.getElementById("formName").value = randomName
  document.getElementById("formEmail").value = randomName.toLowerCase()
      + "@gmail.com"
  document.getElementById("formPhone").value = generateRandomPhoneNumber()
}

function getNewUserParamsFromForm() {
  let params = new Map();
  params.set('name', getHTMLElementValue("formName"));
  params.set('email', getHTMLElementValue("formEmail"));
  params.set('phone', getHTMLElementValue("formPhone"));
  return params;
}

function getNewUserParamsFromTable(row) {
  let id = row.cells[0].textContent;
  let name = row.cells[1].textContent;
  let email = row.cells[2].textContent;
  let phone = row.cells[3].textContent;

  let params = new Map();
  params.set('id', id);
  params.set('name', name);
  params.set('email', email);
  params.set('phone', phone);
  return params;
}

function loadTableData(users) {
  users.forEach(user => {
    insertUser(user)
  });
}

function insertUser(user) {
  let row = usersTable.insertRow();
  let id = row.insertCell(0);
  id.innerHTML = user.id;
  let name = row.insertCell(1);
  name.innerHTML = user.name;
  let email = row.insertCell(2);
  email.innerHTML = user.email;
  let phone = row.insertCell(3);
  phone.innerHTML = user.phone;

  row.insertCell(4).innerHTML =
      '<div style="display: inline-flex;">\n'
      + '        <button type="button" class="btn btn-warning" onclick="editUser(this)" style="margin-right: 10px">Edit</button>\n'
      + '        <button type="button" class="btn btn-danger" onclick="deleteUser(this)">Delete</button>\n'
      + '        <button type="button" style="display: none" class="btn btn-success" onclick="saveEditedUser(this)">Save</button>\n'
      + '      </div>'
}

function editUser(event) {
  let row = event.parentElement.parentElement.parentElement;

  let name = row.cells[1];
  name.contentEditable = 'true';

  let email = row.cells[2];
  email.contentEditable = 'true';

  let phone = row.cells[3];
  phone.contentEditable = 'true';

  row.getElementsByClassName("btn-warning")[0].style.display = 'none';
  row.getElementsByClassName("btn-danger")[0].style.display = 'none';
  row.getElementsByClassName("btn-success")[0].style.display = 'inline-block';

}

function saveEditedUser(event) {
  let row = event.parentElement.parentElement.parentElement;
  executeQuery(
      graphQLMutationConstructor("updateUser", getNewUserParamsFromTable(row),
          ["success", "responseCode"], undefined))
  .then(response => {
    let result = response.updateUser;
    if (result.success) {
      let name = row.cells[1];
      name.contentEditable = 'false';
      let email = row.cells[2];
      email.contentEditable = 'false';
      let phone = row.cells[3];
      phone.contentEditable = 'false';

      row.getElementsByClassName(
          "btn-warning")[0].style.display = 'inline-block';
      row.getElementsByClassName(
          "btn-danger")[0].style.display = 'inline-block';
      row.getElementsByClassName("btn-success")[0].style.display = 'none';
      alert("Edited successfully!");
    } else {
      alert("Edition failed!");
    }
  })

}

function deleteUser(event) {
  let row = event.parentElement.parentElement.parentElement;
  let id = row.cells[0].textContent;

  executeQuery(
      graphQLMutationConstructor("deleteUser", undefined,
          ["success", "responseCode"], id))
  .then(response => {
    let result = response.deleteUser;
    if (result.success) {
      alert("Deleted successfully!");
      event.closest('tr').remove();
    } else {
      alert("Deletion failed!");
    }
  })
}

function addNewUser() {
  if (!getHTMLElementValue("formName") || !getHTMLElementValue("formEmail")
      || !getHTMLElementValue("formPhone")) {
    alert("Check the input data!");
    return;
  }
  executeQuery(graphQLMutationConstructor("newUser", getNewUserParamsFromForm(),
      ["id", "name", "email", "phone"], undefined))
  .then(response => {
    insertUser(response.newUser);
    cleanupNewUserForm();
  })
}

function getAllUsers() {
  executeQuery(
      graphQLQueryConstructor("getAllUsers", undefined,
          ["id", "name", "email", "phone"]))
  .then(response => {
    loadTableData(response.getAllUsers)
  })
}

function cleanupNewUserForm() {
  let container = document.getElementById("newUserForm");
  let inputs = container.getElementsByTagName('input');
  for (let index = 0; index < inputs.length; ++index) {
    inputs[index].value = '';
  }
}